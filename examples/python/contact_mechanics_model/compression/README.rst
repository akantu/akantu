Compression (2D)
''''''''''''''''
:Sources:

   .. collapse:: compression.py (click to expand)

      .. literalinclude:: examples/python/contact_mechanics_model/compression/compression.py
         :language: python
         :lines: 9-

   .. collapse:: compression.dat (click to expand)

      .. literalinclude:: examples/python/contact_mechanics_model/compression/compression.dat
         :language: text

:Location:

   ``examples/python/contact_mechanics_model/`` `compression <https://gitlab.com/akantu/akantu/-/blob/master/examples/python/contact_mechanics_model/compression>`_

In `compression.py` it is shown how to couple the Solid Mechanics Model with the Contact Mechanics Model. The example 
simulate the contact betweem two blocks.

.. figure:: examples/python/contact_mechanics_model/compression/images/compression.svg
            :align: center
            :width: 25%

.. figure:: examples/python/contact_mechanics_model/compression/images/contact.gif
            :align: center
            :width: 50%

